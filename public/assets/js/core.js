(function ($) {
	$(document).ready(function () {
		var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-pagination',
			paginationClickable: true,
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			spaceBetween: 30,
			autoplay: 5000,
			autoplayDisableOnInteraction: false
		});

		$(window).scroll(function () {
			if ($(document).scrollTop() > 50) {
				$('.navbar').addClass('shrink');
				$('.add').hide();
			} else {
				$('.navbar').removeClass('shrink');
				$('.add').show();
			}
		});

		/*$(window).scroll(function () {
			if ($(document).scrollTop() > 50) {
				$('.main__nav__small__device').addClass('shrink__small__view');
				$('.add').hide();
			} else {
				$('.main__nav__small__device').removeClass('shrink__small__view');
				$('.add').show();
			}
		});*/

		/*$('.accordion > dd').hide();
		$('.accordion dt a').click(function (e) {
			event.preventDefault();
			$('.accordion > dd').not($(this).parent().next()).slideUp();
			$(this).toggleClass('active');
			$(this).parent().next().slideToggle();

		});

		$("ul.dropdown-menu [data-toggle='dropdown']").on("click", function (event) {
			event.preventDefault();
			event.stopPropagation();
			var $curr;
			if ($(this).parent().hasClass("dropdown-submenu")) {
				$curr = $(this).parent();
				$super = $curr.parent();
				$super.find(".show").not($curr).removeClass("show");
				$curr.toggleClass("show");
				$curr.find("ul").toggleClass("show");
				(!$curr.hasClass('show')) ? $curr.find("ul").addClass('sub__menu__sub'): $curr.find("ul").removeClass('sub__menu__sub');
			}
		});*/
		
	/*	$(".dropdown a").click(function(){
			$("ul.navbar-nav").not($(this).parent()).find(".dropdown-submenu").removeClass("show");
		});*/
	});
})(jQuery);

