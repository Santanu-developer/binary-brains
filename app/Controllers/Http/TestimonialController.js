'use strict'

const Testimonial = use('App/Models/Testimonial')
const { validate } = use('Validator')

/**
 * Resourceful controller for interacting with testimonials
 */
class TestimonialController {
  /**
   * Show a list of all testimonials.
   * GET testimonials
   */
  async index ({ view }) {
    const testimonials = await Testimonial.all()
    return view.render('admin.testimonial.list', { testimonials: testimonials.toJSON() })
  }

  /**
   * Render a form to be used for creating a new testimonial.
   * GET testimonials/create
   */
  async create ({ view }) {
    return view.render('admin.testimonial.create')
  }

  /**
   * Create/save a new testimonial.
   * POST testimonials
   */
  async store ({ request, response, session }) {

    const rules = {
      title: 'required|string|max:100|min:3',
      content: 'required|string|max:500'
    }

    const validation = await validate(request.all(), rules)

    if (validation.fails()) {
      session
        .withErrors(validation.messages())
        .flashAll()

      return response.redirect('back')
    }


    const testimonial = new Testimonial()

    testimonial.title = request.input('title')
    testimonial.content = request.input('content')

    await testimonial.save()

    session.flash({
      notification:{
          type:'success',
          message:`Testimonial Created Successfully`
        }
    })

    return response.route('testimonial.index')
  }



  /**
   * Render a form to update an existing testimonial.
   * GET testimonials/:id/edit
   */
  async edit ({ params, view }) {
    const testimonial = await Testimonial.findOrFail(params.id)
    return view.render('admin.testimonial.edit', {testimonial: testimonial.toJSON()})
  }

  /**
   * Update testimonial details.
   * PUT or PATCH testimonials/:id
   */
  async update ({ params, request, response, session }) {

    const rules = {
      title: 'required|string|max:100|min:3',
      content: 'required|string|max:500'
    }

    const validation = await validate(request.all(), rules)

    if (validation.fails()) {
      session
        .withErrors(validation.messages())
        .flashAll()

      return response.redirect('back')
    }

    
    const data= request.only(['title','content'])
    const testimonial = await Testimonial.findOrFail(params.id)
    
    if (( data.title === testimonial.title) && (data.content === testimonial.content)) {
      session.flash({
        notification:{
            type:'danger',
            message:`Nothing to save`
          }
      })
    }
    else {
      testimonial.title = data.title
      testimonial.content = data.content

      await testimonial.save()
  
      session.flash({
        notification:{
            type:'success',
            message:`Testimonial Updated Successfully`
          }
      })
    }

    return response.route('testimonial.index')
  }

  /**
   * Delete a testimonial with id.
   * DELETE testimonials/:id
   */
  async destroy ({ params, request, response, session }) {
    const testimonial = await Testimonial.findOrFail(params.id)
    
    await testimonial.delete();

    session.flash({
      notification:{
          type:'danger',
          message:`Testimonial Deleted`
        }
    })

    return response.route('testimonial.index')
  }
}

module.exports = TestimonialController
