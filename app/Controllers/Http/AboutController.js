'use strict'

const About = use('App/Models/About')
const { validate } = use('Validator')

/**
 * Resourceful controller for interacting with abouts
 */
class AboutController {
  /**
   * Show a list of all abouts.
   * GET abouts
   */
  async index ({ view }) {
    const abouts = await About.all()
    return view.render('admin.about.list', { abouts: abouts.toJSON() })
  }


  /**
   * Render a form to update an existing about.
   * GET abouts/:id/edit
   */
  async edit ({ params, view }) {
    const about = await About.findOrFail(params.id)
    return view.render('admin.about.edit', {about: about.toJSON()})
  }

  /**
   * Update about details.
   * PUT or PATCH abouts/:id
   */
  async update ({ params, request, response, session }) {

    const rules = {
      title: 'required|string|max:100|min:3',
      content: 'required|string|max:800'
    }

    const validation = await validate(request.all(), rules)

    if (validation.fails()) {
      session
        .withErrors(validation.messages())
        .flashAll()

      return response.redirect('back')
    }

    const data= request.only(['title','content'])
    const about = await About.findOrFail(params.id)
    //console.log(data)
    
    if (( data.title === about.title) && (data.content === about.content)) {
      session.flash({
        notification:{
            type:'danger',
            message:`Nothing to save`
          }
      })
    }
    else {
      about.title = data.title
      about.content = data.content
  
      await about.save()
  
      session.flash({
        notification:{
            type:'success',
            message:`About Us Updated Successfully`
          }
      })
    }

    return response.route('about.index')
  }

  /**
   * Delete a about with id.
   * DELETE abouts/:id
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = AboutController
