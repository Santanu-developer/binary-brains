'use strict'

const Mail = use('Mail')
const { validate } = use('Validator')

const Banner = use('App/Models/Banner')
const About = use('App/Models/About')
const ServiceText = use('App/Models/ServiceText')
const Service = use('App/Models/Service')
const TestimonialText = use('App/Models/TestimonialText')
const Testimonial = use('App/Models/Testimonial')
const Career = use('App/Models/Career')
const FooterLocation = use('App/Models/FooterLocation')
const FooterContact = use('App/Models/FooterContact')
const FooterPhone = use('App/Models/FooterPhone')
const FooterSocial = use('App/Models/FooterSocial')
const FooterCopyright = use('App/Models/FooterCopyright')
const Portfolio = use('App/Models/Portfolio')

class HomeController {

  async index ({ view }) {
    const banners = await Banner.all()
    const abouts = await About.all()
    const serviceTexts = await ServiceText.all()
    const services = await Service.all()
    const testimonialTexts = await TestimonialText.all()
    const testimonials = await Testimonial.all()
    const careers = await Career.all()

    const footerLocations = await FooterLocation.all()
    const footerContacts = await FooterContact.all()
    const footerPhones = await FooterPhone.all()
    const footerSocials = await FooterSocial.all()
    const footerCopyrights = await FooterCopyright.all()

    const portfolios = await Portfolio.all()
    const portfolioGraphics = await Portfolio.query().where('category', 'GD').fetch()
    const portfolioWebs = await Portfolio.query().where('category', 'WD').fetch()
    const portfolioMobiles = await Portfolio.query().where('category', 'MAD').fetch()
    const portfolioDevlopments = await Portfolio.query().where('category', 'WAD').fetch()


    return view.render('frontend.home', 
    { 
      banners: banners.toJSON(), 
      abouts: abouts.toJSON(), 
      serviceTexts: serviceTexts.toJSON(), 
      services: services.toJSON(), 
      testimonialTexts: testimonialTexts.toJSON(), 
      testimonials:testimonials.toJSON(), 
      careers:careers.toJSON(), 
      footerLocations:footerLocations.toJSON(), 
      footerContacts:footerContacts.toJSON(), 
      footerPhones:footerPhones.toJSON(), 
      footerSocials:footerSocials.toJSON(), 
      footerCopyrights: footerCopyrights.toJSON(), 
      portfolios: portfolios.toJSON(),
      portfolioGraphics: portfolioGraphics.toJSON(),
      portfolioWebs: portfolioWebs.toJSON(),
      portfolioMobiles: portfolioMobiles.toJSON(),
      portfolioDevlopments: portfolioDevlopments.toJSON()
    })
  }


  async sendMail({ request, session, response }) {

    const rules = {
      email: 'required|email',
      name: 'required|string|max:50',
      phone: 'required|max:12',
      message: 'string|max:200'
    }

    const validation = await validate(request.all(), rules)

    if (validation.fails()) {
      session
        .withErrors(validation.messages())
        .flashAll()

      return response.redirect('back')
    }

    try {
      const data = request.only(['email', 'name', 'phone', 'message'])
      //const user = await User.create(data)
  
      await Mail.send('emails.contact', data, (message) => {
        message
          .to('admin@binarybrains.com')
          .from(data.emai)
          .subject('Contact Form Enquiry Mail')
      })
  
      session.flash({
        notification:{
            type:'success',
            message:`Message Sent Successfully`
          }
      })
  
      return response.route('home')
      
    } catch (error) {
      
      session.flash({
        notification:{
            type:'danger',
            message:`Sorry, Error in Message Sending!!`
          }
      })
    }

   
  }

}

module.exports = HomeController
