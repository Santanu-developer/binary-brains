'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AboutSchema extends Schema {
  up () {
    this.create('abouts', (table) => {
      table.increments()
      table.string('title', 100)
      table.text('content')
      table.string('image1')
      table.string('image2')
      table.timestamps()
    })
  }

  down () {
    this.drop('abouts')
  }
}

module.exports = AboutSchema
