'use strict'

/*
|--------------------------------------------------------------------------
| AboutSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const About = use('App/Models/About')

class AboutSeeder {
  async run () {
    const abouts = [
      {
        title:'GROW YOUR BUSINESS WITH US AND MAKE MORE PROFIT',
        content:'Glue with us to Gain a hike in your digital status and Grow your Business. 3G Media Solutions is having a consistent track record in providing efficient and effective web-solutions across the globe since incorporation.',
        image1:'',
        image2:''
      }
  ]
    await About.createMany(abouts)
  }
}

module.exports = AboutSeeder
