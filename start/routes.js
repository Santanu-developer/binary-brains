'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', 'HomeController.index').as('home')
Route.get('home', 'HomeController.index')

Route.post('sendMail', 'HomeController.sendMail').as('sendMail')


Route
  .group(() => {
    Route.get('login', 'Auth/LoginController.showLogin').middleware(['guest'])
    Route.get('/', 'Auth/LoginController.showLogin').as('showLogin').middleware(['guest'])
    Route.post('login', 'Auth/LoginController.login').as('login')
    Route.post('logout', 'Auth/LogoutController.logout').as('logout')
    Route.on('dashboard').render('admin.dashboard').as('adminHome').middleware('auth')

    Route.resource('banner', 'BannerController').except(['store','create','show']).middleware(['auth'])
    Route.resource('about', 'AboutController').except(['store','create','show']).middleware(['auth'])
    Route.resource('serviceText', 'ServiceTextController').except(['store','create','show']).middleware(['auth'])
    Route.resource('service', 'ServiceController').except(['show']).middleware(['auth'])
    Route.resource('testimonialText', 'TestimonialTextController').except(['store','create','show']).middleware(['auth'])
    Route.resource('testimonial', 'TestimonialController').except(['show']).middleware(['auth'])
    Route.resource('career', 'CareerController').except(['show']).middleware(['auth'])
    Route.resource('portfolio', 'PortfolioController').except(['show']).middleware(['auth'])

    
    Route.resource('footerLocation', 'FooterLocationController').except(['show']).middleware(['auth'])
    Route.resource('footerContact', 'FooterContactController').except(['show']).middleware(['auth'])
    Route.resource('footerPhone', 'FooterPhoneController').except(['show']).middleware(['auth'])
    Route.resource('footerSocial', 'FooterSocialController').except(['show']).middleware(['auth'])
    Route.resource('footerCopyright', 'FooterCopyrightController').except(['store','create','show']).middleware(['auth'])
  })
  .prefix('admin')